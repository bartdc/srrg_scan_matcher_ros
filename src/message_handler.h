#pragma once

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Imu.h>

#include <tf/transform_listener.h>

#include "cloud2d.h"
#include "projector2d.h"

#include "srrg_messages/laser_message.h"


using namespace std;
using namespace srrg_core;
 using namespace srrg_scan_matcher;

struct CloudWithTime: public Cloud2D {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  CloudWithTime(const ros::Time& t, const Eigen::Isometry2f& guess_=Eigen::Isometry2f::Identity()){
    timestamp=t;
    guess=guess_;
  }
  ros::Time timestamp;
  Eigen::Isometry2f guess;
  LaserMessage* laser;
};

typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, sensor_msgs::LaserScan> MyOdomScanSyncPolicy;

class MessageHandler {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
  MessageHandler(tf::TransformListener* listener_);
  ~MessageHandler();
  void laser_callback(const sensor_msgs::LaserScan::ConstPtr& msg);  
  void imu_callback(const sensor_msgs::Imu::ConstPtr& msg);  
  void odom_laser_callback(const nav_msgs::Odometry::ConstPtr& odommsg, const sensor_msgs::LaserScan::ConstPtr& scanmsg);  

  // setter & getter
  inline void setFrameSkip(int frame_skip) { _frame_skip = frame_skip; }
  inline int frameSkip() const { return _frame_skip; }
  inline int laserFrameCount() const { return _frame_count; }
  inline std::list<CloudWithTime*>& clouds()  { return _clouds; }
  inline const Projector2D* projector() const { return _projector; }
  inline Eigen::Isometry2f laserOffset() { return _laser_offset;}
  inline void setBaseLinkFrame(const std::string base_link_frame_id_){_base_link_frame_id = base_link_frame_id_;}
  inline void setLaserTopic(const std::string laser_topic_){_laser_topic = laser_topic_;}
  

private:

  void lookForLaserOffset(const sensor_msgs::LaserScan::ConstPtr& scanmsg, Eigen::Isometry3f& offset3f);
  LaserMessage* LaserScanToLaserMessage(const sensor_msgs::LaserScan::ConstPtr& scanmsg);

  tf::TransformListener* _listener;
  Projector2D* _projector;
  std::list<CloudWithTime*> _clouds;
  sensor_msgs::Imu _last_imu_msg;
  Eigen::Isometry2f _laser_offset;

  std::string _base_link_frame_id;
  std::string _laser_topic;

  int _frame_count;
  int _frame_skip;
};
