#include "message_handler.h"

MessageHandler::MessageHandler(tf::TransformListener* listener_) {
  _listener = listener_;
  _projector = NULL;
  _frame_count = 0;
  _frame_skip = 1;
  _last_imu_msg.header.stamp=ros::Time(0.0d);

  _laser_offset.setIdentity();
}

MessageHandler::~MessageHandler() {
  if (_projector)
    delete _projector;
}


void MessageHandler::lookForLaserOffset(const sensor_msgs::LaserScan::ConstPtr& scanmsg, Eigen::Isometry3f& offset3f){
  offset3f.setIdentity();
  tf::StampedTransform transform;
  transform.setIdentity();
  if (_listener){
    try{
      _listener->waitForTransform(_base_link_frame_id, scanmsg->header.frame_id, 
				  scanmsg->header.stamp, 
				  ros::Duration(0.1) );
      _listener->lookupTransform (_base_link_frame_id, scanmsg->header.frame_id, 
      				  scanmsg->header.stamp, 
				  transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
    }
    Eigen::Quaternionf q;
    q.x() = transform.getRotation().x();
    q.y() = transform.getRotation().y();
    q.z() = transform.getRotation().z();
    q.w() = transform.getRotation().w();
    _laser_offset.setIdentity();
    _laser_offset.translation() = Eigen::Vector2f(transform.getOrigin().x(),
						  transform.getOrigin().y());
    Eigen::Matrix3f offset_rotation = q.toRotationMatrix();
    _laser_offset.linear() = offset_rotation.block<2,2>(0,0);

    offset3f.linear() = offset_rotation;
    offset3f.translation() = Eigen::Vector3f(transform.getOrigin().x(),
					     transform.getOrigin().y(), 
					     transform.getOrigin().z());
  }

}


LaserMessage* MessageHandler::LaserScanToLaserMessage(const sensor_msgs::LaserScan::ConstPtr& scanmsg){

  LaserMessage* laser=new LaserMessage(_laser_topic, scanmsg->header.frame_id,  scanmsg->header.seq, scanmsg->header.stamp.toSec());

  laser->setMinAngle(scanmsg->angle_min);
  laser->setMaxAngle(scanmsg->angle_max);
  laser->setAngleIncrement(scanmsg->angle_increment);
  laser->setMinRange(scanmsg->range_min);
  laser->setMaxRange(scanmsg->range_max);
  laser->setTimeIncrement(scanmsg->time_increment);
  laser->setScanTime(scanmsg->scan_time);

  std::vector<float> ranges;
  ranges.resize(scanmsg->ranges.size());
  for (size_t i = 0; i < scanmsg->ranges.size(); i++){
    if ((scanmsg->ranges[i] < scanmsg->range_min) || std::isnan(scanmsg->ranges[i]))
      ranges[i]=0;
    else if (scanmsg->ranges[i] > scanmsg->range_max)
      ranges[i]=scanmsg->range_max;
    else
      ranges[i]=scanmsg->ranges[i];
  }
  laser->setRanges(ranges);

  std::vector<float> intensities;
  intensities.resize(scanmsg->intensities.size());
  for (size_t i = 0; i < scanmsg->intensities.size(); i++)
    intensities[i]=scanmsg->intensities[i];
  laser->setIntensities(intensities);

  return laser;
}


void MessageHandler::laser_callback(const sensor_msgs::LaserScan::ConstPtr& msg) {
  ++_frame_count;

  if (_frame_count % _frame_skip != 0)
    return;

  if (_projector == NULL) {
    float fov = 0.f;
    fov = msg->angle_increment * (msg->ranges.size()-1);
    _projector = new Projector2D();
    _projector->setMaxRange(msg->range_max);
    _projector->setMinRange(msg->range_min);
    _projector->setFov(fov);
    std::cerr << " ranges size" << msg->ranges.size() << endl;
    std::cerr << " maxangle" << msg->angle_max *180/M_PI << endl;
    std::cerr << " minangle" << msg->angle_min *180/M_PI << endl;
    std::cerr << " angleinc" << msg->angle_increment *180/M_PI << endl;

    _projector->setNumRanges(msg->ranges.size());
  }

  Eigen::Isometry2f guess;
  guess.setIdentity();
  if (_last_imu_msg.header.stamp.toSec()>0) {
    guess.translation().setZero();
    float angle=Eigen::AngleAxisf(Eigen::Quaternionf(_last_imu_msg.orientation.x,
						     _last_imu_msg.orientation.y,
						     _last_imu_msg.orientation.z,
						     _last_imu_msg.orientation.w).toRotationMatrix()).angle();
    guess.linear()=Eigen::Rotation2Df(angle).toRotationMatrix();
  }

  //Getting laser offset
  Eigen::Isometry3f offset3f = Eigen::Isometry3f::Identity();
  lookForLaserOffset(msg, offset3f);

  //Transforming scan into a LaserMessage
  LaserMessage* laser = LaserScanToLaserMessage(msg);
  laser->setOffset(offset3f);

  //Creating cloud
  CloudWithTime* current = new CloudWithTime(msg->header.stamp, guess);
  current->laser = laser;
  _projector->unproject(*current, msg->ranges);
  _clouds.push_back(current);
}

void MessageHandler::imu_callback(const sensor_msgs::Imu::ConstPtr& msg) {
  _last_imu_msg= *msg;
}

void MessageHandler::odom_laser_callback(const nav_msgs::Odometry::ConstPtr& odommsg, const sensor_msgs::LaserScan::ConstPtr& scanmsg) {

  ++_frame_count;

  if (_frame_count % _frame_skip != 0)
    return;

  if (_projector == NULL) {
    float fov = 0.f;
    fov = scanmsg->angle_increment * (scanmsg->ranges.size()-1);
    _projector = new Projector2D();
    _projector->setMaxRange(scanmsg->range_max);
    _projector->setMinRange(scanmsg->range_min);
    _projector->setFov(fov);
    std::cerr << " ranges size" << scanmsg->ranges.size() << endl;
    std::cerr << " maxangle" << scanmsg->angle_max *180/M_PI << endl;
    std::cerr << " minangle" << scanmsg->angle_min *180/M_PI << endl;
    std::cerr << " angleinc" << scanmsg->angle_increment *180/M_PI << endl;


    _projector->setNumRanges(scanmsg->ranges.size());
  }

  Eigen::Isometry2f curr_odom;
  Eigen::Vector2f pose(odommsg->pose.pose.position.x, odommsg->pose.pose.position.y);
  curr_odom.translation() = pose;
  Eigen::Quaternionf pose_q;
  pose_q.x() = odommsg->pose.pose.orientation.x;
  pose_q.y() = odommsg->pose.pose.orientation.y;
  pose_q.z() = odommsg->pose.pose.orientation.z;
  pose_q.w() = odommsg->pose.pose.orientation.w;
  Eigen::Matrix3f pose_rotation = pose_q.toRotationMatrix();
  curr_odom.linear()=pose_rotation.block<2,2>(0,0);

  //Getting laser offset
  Eigen::Isometry3f offset3f = Eigen::Isometry3f::Identity();
  lookForLaserOffset(scanmsg, offset3f);

  //Transforming scan into a LaserMessage
  LaserMessage* laser = LaserScanToLaserMessage(scanmsg);
  laser->setOffset(offset3f);

  //Creating cloud
  CloudWithTime* current = new CloudWithTime(scanmsg->header.stamp, curr_odom);
  current->laser = laser;
  _projector->unproject(*current, scanmsg->ranges);
  _clouds.push_back(current);
}
