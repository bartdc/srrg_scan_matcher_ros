#include <fstream>
#include <sstream>

// ROS
#include "ros/ros.h"
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include "message_filters/sync_policies/approximate_time.h"
#include "std_msgs/String.h"
#include "tf/transform_broadcaster.h"

// thin_navigation_core
#include "cloud2d.h"
#include "projector2d.h"
#include "projective_correspondence_finder2d.h"
#include "cloud_processor.h"
#include "solver2d.h"
#include "tracker2d.h"

#include "message_handler.h"

//interface
#include <QApplication>
#include "tracker_viewer.h"

using namespace std;
using namespace srrg_core;
using namespace srrg_scan_matcher;
using namespace srrg_scan_matcher_gui;

// ugly program parameters

bool use_gui;
bool use_odom;
std::string laser_topic;
std::string imu_topic;
std::string odom_topic;
std::string published_odom_topic;
std::string published_tf_origin_frame_id;
std::string published_tf_destination_frame_id;
std::string base_link_frame_id;
std::string cfinder;
double inlier_distance;
int frame_skip;
double bpr;
bool publish_odom;
bool publish_tf;
double merging_distance, merging_normal_angle;
double voxelize_res;
double min_correspondences_ratio;
int iterations;

double tracker_damping;

double local_map_clipping_range;
double local_map_clipping_translation_threshold;

double max_matching_range;
double min_matching_range;
int num_matching_beams;
double matching_fov;
bool projective_merge;
string dump;
bool verbose;

double laser_translation_threshold;
double laser_rotation_threshold;

MessageHandler* message_handler;



void readParameters(ros::NodeHandle& n){
  // ROS parameters setting
  n.param("laser_topic", laser_topic, std::string("/scan"));
  n.param("imu_topic", imu_topic, std::string(""));
  n.param("odom_topic", odom_topic, std::string("/odom"));
  n.param("use_odom", use_odom, false);
  n.param("published_odom_topic", published_odom_topic, std::string("/odom_calib"));
  n.param("base_link_frame_id", base_link_frame_id, std::string("/base_link"));
  n.param("published_tf_origin_frame_id", published_tf_origin_frame_id, std::string("/thin_navigation_frame"));
  n.param("published_tf_destination_frame_id", published_tf_destination_frame_id, std::string("/base_link"));
  n.param("publish_odom", publish_odom, true);
  n.param("publish_tf", publish_tf, false);
  n.param("inlier_distance", inlier_distance, 0.3);
  n.param("iterations", iterations, 10);
  n.param("frame_skip", frame_skip, 1);
  n.param("bpr", bpr, 0.4);
  n.param("min_correspondences_ratio", min_correspondences_ratio, 0.5);
  n.param("local_map_clipping_range", local_map_clipping_range, 10.0);
  n.param("local_map_clipping_translation_threshold", local_map_clipping_translation_threshold, 5.0);
  n.param("max_matching_range", max_matching_range, 0.0);
  n.param("min_matching_range", min_matching_range, 0.0);
  n.param("num_matching_beams", num_matching_beams, 0);
  n.param("merging_distance", merging_distance, 0.2);
  n.param("merging_normal_angle", merging_normal_angle, 1.0);
  n.param("voxelize_res", voxelize_res, 0.0);
  n.param("matching_fov", matching_fov, 0.0);
  n.param("use_gui", use_gui, false);
  n.param("projective_merge", projective_merge, true);
  n.param("dump", dump, std::string(""));
  n.param("tracker_damping", tracker_damping, 1.0);
  n.param("cfinder", cfinder, std::string("projective"));
  n.param("verbose", verbose, false);
  
  n.param("laser_translation_threshold", laser_translation_threshold, -1.0);
  n.param("laser_rotation_threshold", laser_rotation_threshold, -1.0);
  //TODO: odom_weights

  if (frame_skip <= 0)
    frame_skip = 1;

  std::cout << "Launched with params:" << std::endl;
  std::cout << "_laser_topic:=" << laser_topic << std::endl;
  std::cout << "_imu_topic:=" << imu_topic << std::endl;
  std::cout << "_odom_topic:=" << odom_topic << std::endl;
  std::cout << "_use_odom:=" << (use_odom?"true":"false") << std::endl;
  std::cout << "_published_odom_topic:=" << published_odom_topic << std::endl;
  std::cout << "_frame_skip:=" << frame_skip << std::endl;
  std::cout << "_bpr:=" << bpr << std::endl;
  std::cout << "_base_link_frame_id:=" << base_link_frame_id << std::endl;
  std::cout << "_published_tf_origin_frame_id:=" << published_tf_origin_frame_id << std::endl;
  std::cout << "_published_tf_destination_frame_id:=" << published_tf_destination_frame_id << std::endl;
  std::cout << "_publish_odom:=" << publish_odom << std::endl;
  std::cout << "_publish_tf:=" << publish_tf << std::endl;
  std::cout << "_inlier_distance:=" << inlier_distance << std::endl;
  std::cout << "_min_correspondences_ratio:=" << min_correspondences_ratio << std::endl;
  std::cout << "_local_map_clipping_range:=" << local_map_clipping_range << std::endl;
  std::cout << "_local_map_clipping_translation_threshold:=" << local_map_clipping_translation_threshold << std::endl;
  std::cout << "_max_matching_range:=" << max_matching_range << std::endl;
  std::cout << "_min_matching_range:=" << min_matching_range << std::endl;
  std::cout << "_num_matching_beams:=" << num_matching_beams << std::endl;
  std::cout << "_matching_fov:=" << matching_fov << std::endl;
  std::cout << "_merging_distance:=" << merging_distance << std::endl;
  std::cout << "_merging_normal_angle:=" << merging_normal_angle << std::endl;
  std::cout << "_voxelize_res:=" << voxelize_res << std::endl;
  std::cout << "_iterations:=" <<  iterations << std::endl;
  std::cout << "_use_gui:=" <<  (use_gui?"true":"false") << std::endl;
  std::cout << "_projective_merge:=" <<  (projective_merge?"true":"false") << std::endl;
  std::cout << "_dump:=" <<  dump << std::endl;
  std::cout << "_tracker_damping " << tracker_damping << std::endl;
  std::cout << "_cfinder:=" <<  cfinder << std::endl;
  std::cout << "_verbose:=" <<  (verbose?"true":"false") << std::endl;
  std::cout << "_laser_translation_threshold:=" <<  laser_translation_threshold << std::endl;
  std::cout << "_laser_rotation_threshold:=" <<  laser_rotation_threshold << std::endl;
  
  fflush(stdout);

}


Projector2D* initializeProjectorFromParameters(){
  
  const Projector2D* mh_projector = message_handler->projector();
  Projector2D* projector = new Projector2D;

  if (max_matching_range != 0)
    projector->setMaxRange(max_matching_range);
  else
    projector->setMaxRange(mh_projector->maxRange());

  if (min_matching_range != 0)
    projector->setMinRange(min_matching_range);
  else
    projector->setMinRange(mh_projector->minRange());

  if (num_matching_beams != 0)
    projector->setNumRanges(num_matching_beams);
  else
    projector->setNumRanges(mh_projector->numRanges());

  if (matching_fov != 0)
    projector->setFov(matching_fov);
  else
    projector->setFov(mh_projector->fov());
  
  return projector;
}


  Projector2D* projector = 0;
  Tracker2D* tracker = new Tracker2D(); 
  ProjectiveCorrespondenceFinder2D* projective_finder = new ProjectiveCorrespondenceFinder2D;
  NNCorrespondenceFinder2D* nn_finder = new NNCorrespondenceFinder2D;

int main(int argc, char **argv) {

  ros::init(argc, argv, "srrg_scan_matcher_node");
  ros::NodeHandle n("~");
  readParameters(n);
  
  tf::TransformListener *listener = new tf::TransformListener();
  message_handler = new MessageHandler(listener);

  // Setting input parameters
  message_handler->setFrameSkip(frame_skip);
  message_handler->setBaseLinkFrame(base_link_frame_id);

  // ROS topic subscriptions
  ros::Subscriber laser_sub;
  message_filters::Subscriber<nav_msgs::Odometry> subSyncOdom(n, odom_topic, 1);
  message_filters::Subscriber<sensor_msgs::LaserScan> subSyncScan(n, laser_topic, 1);

  message_filters::Synchronizer<MyOdomScanSyncPolicy> syncOdomScan(MyOdomScanSyncPolicy(10), subSyncOdom, subSyncScan);
  if (!use_odom)
    laser_sub = n.subscribe(laser_topic, 1, &MessageHandler::laser_callback, message_handler);
  else {
    syncOdomScan.registerCallback(boost::bind(&MessageHandler::odom_laser_callback, message_handler, _1, _2));
  }
  ros::Subscriber imu_sub;
  if (imu_topic!="")
    imu_sub = n.subscribe(imu_topic, 1, &MessageHandler::imu_callback, message_handler);

  ros::Publisher odom_pub = n.advertise<nav_msgs::Odometry>(published_odom_topic, 1);

  if (cfinder=="projective")
    tracker->setCorrespondenceFinder(projective_finder);
  else if (cfinder=="nn")
    tracker->setCorrespondenceFinder(nn_finder);
  else {
    cerr << "unknown correspondence finder type: [" << cfinder << "]" << endl;
    return -1;
  }

  tracker->setVerbose(verbose);
  tracker->setBpr(bpr);
  tracker->setIterations(iterations);
  tracker->setInlierDistance(inlier_distance);
  tracker->setMinCorrespondencesRatio(min_correspondences_ratio);
  tracker->setLocalMapClippingRange(local_map_clipping_range);
  tracker->setClipTranslationThreshold(local_map_clipping_translation_threshold);
  tracker->setVoxelizeResolution(voxelize_res);
  tracker->setMergingDistance(merging_distance);
  tracker->setMergingNormalAngle(merging_normal_angle);
  tracker->setEnableProjectiveMerge(projective_merge);
  tracker->setDumpFilename(dump);
  std::cerr << "Tracker2D allocated" << std::endl;
  std::cerr << "inlier distance from tracker: "<< tracker->inlierDistance() << std::endl;

  CloudWithTime *cloud = 0;
  Eigen::Isometry2f global_t;

  std::cerr << "Entering ROS loop" << std::endl;
  
  QApplication* app=0;
  Tracker2DViewer* viewer=0;
  if (use_gui) {
    app=new QApplication(argc, argv);
    viewer=new Tracker2DViewer(tracker);
    viewer->init();
    viewer->show();
  }
  Eigen::Isometry2f previous_guess;
  bool has_guess=false;

  
  ros::Rate loop_rate(50);
  while (ros::ok()) {
    std::list<CloudWithTime*>& clouds = message_handler->clouds();

    float q_size=clouds.size();
    while (clouds.size() > 0) {
      if (projector == 0) {
	cerr << "projector initialized" << endl;
	projector = initializeProjectorFromParameters();
	tracker->setProjector(projector);
      }
      
      CloudWithTime* cloud = clouds.front();
      ros::Time timestamp = cloud->timestamp;
      clouds.pop_front();

      Eigen::Isometry2f laser_offset = message_handler->laserOffset();
      //cloud->transformInPlace(laser_offset);
      if(has_guess) {
	Eigen::Isometry2f odom_prior_T=previous_guess.inverse()*cloud->guess;
	Eigen::Matrix3f odom_info;
	odom_info.setIdentity();
	float alpha1=10;
	float alpha2=10;
	float alpha3=10;
	Eigen::Vector3f odom_prior=t2v(odom_prior_T);
	odom_info(0,0)=alpha1/(1e-3 + fabs(odom_prior.x()));
	odom_info(1,1)=alpha2/(1e-3 + fabs(odom_prior.y()));
	odom_info(2,2)=alpha3/(1e-3 + fabs(odom_prior.z()));

	//skipping lasers if we didn't move enough
	//this condition has sense if use_odom is true, if false we always do the track
	if (!use_odom ||
	    (use_odom && (odom_prior_T.translation().norm() > laser_translation_threshold 
			  || fabs(odom_prior.z()) > laser_rotation_threshold))) {

	  tracker->update(cloud, laser_offset.inverse()*odom_prior_T*laser_offset, odom_info);

	}else {
	  std::cerr << "Laser SKIPPED" << std::endl;
	  continue;
	}
      } else {
	tracker->update(cloud);
	has_guess=true;
      }
      previous_guess=cloud->guess;

      global_t = tracker->globalT();

      Eigen::Matrix3f rot = Eigen::Matrix3f::Identity();
      rot.block<2,2>(0, 0) = global_t.linear();
      Eigen::Quaternionf q(rot);
      q.normalize();

      if (publish_odom) {
	nav_msgs::Odometry odom_msg;
	
	odom_msg.header.stamp = timestamp;
	odom_msg.header.frame_id = base_link_frame_id;
	odom_msg.pose.pose.position.x = global_t.translation().x();
	odom_msg.pose.pose.position.y = global_t.translation().y();
	odom_msg.pose.pose.position.z = 0;
	odom_msg.pose.pose.orientation.w = q.w();
	odom_msg.pose.pose.orientation.x = q.x();
	odom_msg.pose.pose.orientation.y = q.y();
	odom_msg.pose.pose.orientation.z = q.z();
 
	odom_pub.publish(odom_msg);
      }

      if (publish_tf) {
	static tf::TransformBroadcaster tf_broadcaster;

	tf::Vector3 tf_translation(global_t.translation().x(), global_t.translation().y(), 0);

	tf::Quaternion tf_quaternion(q.x(), q.y(), q.z(), q.w());

	tf::Transform tf_content;
	tf_content.setOrigin(tf_translation);
	tf_content.setRotation(tf_quaternion);

	tf::StampedTransform tf_msg(tf_content, timestamp, published_tf_origin_frame_id, published_tf_destination_frame_id);
	tf_broadcaster.sendTransform(tf_msg);
      }
    }

    loop_rate.sleep();
    ros::spinOnce();
    if (use_gui) {
      viewer->updateGL();
      app->processEvents();
      QKeyEvent* event=viewer->lastKeyEvent();
      if (event) {
	if (event->key()==Qt::Key_F) {
	  viewer->setFollowRobotEnabled(!viewer->followRobotEnabled());
	  viewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_B) {
	  viewer->setDrawRobotEnabled(!viewer->drawRobotEnabled());
	  viewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_R) {
	  tracker->reset();
	  viewer->keyEventProcessed();
	}
	if (event->key()==Qt::Key_H) {
	  cerr << "HELP" << endl;
	  cerr << "R: reset tracker" << endl;
	  cerr << "B: draws robot" << endl;
	  cerr << "F: enables/disables robot following" << endl;
	  viewer->keyEventProcessed();
	}

      }
    }

    if (verbose) {
      cerr << "Max freq: " << 1.0/tracker->cycleTime()	\
	   << " q_size:" << q_size
	   << " ref_pts: ";
      if (tracker->reference()){
	cerr << tracker->reference()->size();
     
      } else {
	cerr << "0";
      }
      cerr << endl;
    }
    
  }

  return 0;
}
